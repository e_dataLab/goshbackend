from django.apps import AppConfig


class GoshappConfig(AppConfig):
    name = 'goshApp'
