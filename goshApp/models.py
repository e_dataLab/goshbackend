from django.db import models

# Create your models here.

class Organization(models.Model):
    organization_Identifier = models.CharField(max_length=80, default='')
    organization_Type = models.CharField(max_length=80, default='')
    organization_Name = models.CharField(max_length=80, default='')
    organization_Address_Line_1 = models.CharField(max_length=80, default='')
    organization_Address_Line_2 = models.CharField(max_length=80, default='')
    organization_City = models.CharField(max_length=80, default='')
    organization_Post_Code = models.CharField(max_length=80, default='')
    
    def __str__(self):
        return self.organization_Name + ' ' + self.organization_Type

class Practitioner(models.Model):
    authorized_name = models.CharField(max_length=80)
    authorized_role = models.CharField(max_length=80)
    authorized_date = models.DateField()
    reporter_name = models.CharField(max_length=80)
    reporter_role = models.CharField(max_length=80)
    report_date = models.DateField()
    performing_Laboratory = models.ForeignKey(Organization, related_name='practitions', on_delete=models.CASCADE, default='')

    def __str__(self):
        return self.authorized_name + ' ' + self.authorized_role 

class Patient(models.Model):
    first_Name = models.CharField(max_length=80)
    last_Name = models.CharField(max_length=80)
    gender = models.CharField(max_length=80)
    birthdate = models.DateField()
    family_number = models.CharField(max_length=20)
    practitioner = models.ForeignKey(Practitioner, related_name='patients', on_delete=models.CASCADE)

    def __str__(self):
        return self.first_Name + ' ' + self.last_Name 
    
class Specimen(models.Model):
    specimen_type = models.CharField(max_length=80)
    specimen_id = models.CharField(max_length=80)
    specimen_recieved_time = models.DateTimeField(auto_now=False)
    specimen_authorised_time = models.DateTimeField(auto_now=False)
    specimen_collection_time = models.DateTimeField(auto_now=False)
    patient = models.ForeignKey(Patient, related_name='specimens', on_delete=models.CASCADE)

    def __str__(self):
        return self.specimen_id

class GenePanel(models.Model):
    # test_Name = models.CharField(max_length=256)
    gene_Name = models.CharField(max_length=10, default='')
    zygosity = models.CharField(max_length=15)
    inheritance = models.CharField(max_length=30)
    hgvs_Description = models.CharField(max_length=40)
    classification = models.CharField(max_length=30)
    confirmed_Mutation = models.BooleanField(default=False)
    evidence_of_Classification = models.TextField(null=True, max_length=2048)
    # gene_Description = models.TextField(null=True, max_length=2048)
    # gene_References = models.TextField(null=True, max_length=256)
    comments = models.TextField(null=True, max_length=256)
    subject = models.ForeignKey(Patient, related_name='genepanels', on_delete=models.CASCADE)
    specimen = models.ForeignKey(Specimen, related_name='genepanels', on_delete=models.CASCADE)
    practitioner = models.ForeignKey(Practitioner, related_name='genepanels', on_delete=models.CASCADE)

    def __str__(self):
        return self.gene_Name + ' ' + self.zygosity

class OverallInterpretation(models.Model):
    report_Summary = models.TextField(max_length=1024, default='')
    subject = models.ForeignKey(Patient, related_name='overalls', on_delete=models.CASCADE)

    def __str__(self):
        return self.report_Summary

class Recommendation(models.Model):
    recommendation = models.TextField(max_length=1024)
    subject = models.ForeignKey(Patient, related_name='recommends', on_delete=models.CASCADE)

class ReferalReason(models.Model):
    referal_Reason = models.TextField(max_length=1024)
    subject = models.ForeignKey(Patient, related_name='referals', on_delete=models.CASCADE)
    performer = models.ForeignKey(Practitioner, related_name='referals', on_delete=models.CASCADE)

    def __str__(self):
        return self.referal_Reason

class TestMethod(models.Model):
    test_Method = models.TextField(max_length=2048)
    gene_Information = models.TextField(max_length=1024)
    gene_References = models.TextField(null=True, max_length=1024)
    gene_Interpretation = models.ForeignKey(OverallInterpretation, related_name='testmethods', on_delete=models.CASCADE)
    subject = models.ForeignKey(Patient, related_name='testmethods', on_delete=models.CASCADE)

class DiagnosticReport(models.Model):
    report_Title = models.CharField(max_length=80, default='')
    conclusion = models.CharField(max_length=80, default='')
    derived_From = models.ForeignKey(GenePanel, related_name='diagnosticreports', on_delete=models.CASCADE)
    subject = models.ForeignKey(Patient, related_name='diagnosticreports', on_delete=models.CASCADE)
    performer = models.ForeignKey(Practitioner, related_name='diagnosticreports', on_delete=models.CASCADE)
    specimen = models.ForeignKey(Specimen, related_name='diagnosticreports', on_delete=models.CASCADE)

    def __str__(self):
        return self.conclusion

        
import uuid
class File(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(blank=False, null=False)

    def __str__(self):
        return self.file.name