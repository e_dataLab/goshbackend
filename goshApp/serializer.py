from .models import Patient, Practitioner, Organization, Specimen, GenePanel, OverallInterpretation, DiagnosticReport, Recommendation, ReferalReason, TestMethod, File

from rest_framework import serializers

class TestMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestMethod
        fields = '__all__'

class ReferalReasonSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferalReason
        fields = '__all__'

class RecommendationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recommendation
        fields = '__all__'

class DiagnosticReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiagnosticReport
        fields = '__all__'

class OverallInterpretationSerializer(serializers.ModelSerializer):
    testmethods  = TestMethodSerializer(read_only=True, many=True)   
    class Meta:
        model = OverallInterpretation
        fields = '__all__'

class GenePanelSerializer(serializers.ModelSerializer):
    # diagnosticreports  = DiagnosticReportSerializer(read_only=True, many=True)   
    class Meta:
        model = GenePanel
        fields = '__all__'

class SpecimenSerializer(serializers.ModelSerializer):
    diagnosticreports  = DiagnosticReportSerializer(read_only=True, many=True)
    # overalls = OverallInterpretationSerializer(read_only=True, many=True)
    genepanels  = GenePanelSerializer(read_only=True, many=True)
    class Meta:
        model = Specimen
        fields = '__all__'
        
class PatientSerializer(serializers.ModelSerializer):   
    specimens  = SpecimenSerializer(read_only=True, many=True)
    genepanels  = GenePanelSerializer(read_only=True, many=True)
    referals = ReferalReasonSerializer(read_only=True, many=True)
    recommends = RecommendationSerializer(read_only=True, many=True)
    testmethods = TestMethodSerializer(read_only=True, many=True)
    diagnosticreports  = DiagnosticReportSerializer(read_only=True, many=True) 
    overalls = OverallInterpretationSerializer(read_only=True, many=True)
    class Meta:
        model = Patient
        fields = '__all__'

class PractitionerSerializer(serializers.ModelSerializer):
    patients = PatientSerializer(read_only=True, many=True)
    # diagnosticreports  = DiagnosticReportSerializer(read_only=True, many=True) 
    genepanels  = GenePanelSerializer(read_only=True, many=True)
    referals = ReferalReasonSerializer(read_only=True, many=True)
    class Meta:
        model = Practitioner
        fields = '__all__'

class OrganizationSerializer(serializers.ModelSerializer):
    practitions  = PractitionerSerializer(read_only=True, many=True)
    class Meta:
        model = Organization
        fields = '__all__'

class FileSerializers(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"