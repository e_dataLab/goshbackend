from import_export import resources, widgets, fields
from . models import Patient, Organization, Practitioner, Specimen, GenePanel, OverallInterpretation, DiagnosticReport, Recommendation, ReferalReason, TestMethod

class CharRequiredWidget(widgets.CharWidget):
    def clean(self, value, row=None, *args, **kwargs):
        val = super().clean(value)
        if val:
            return val
        else:
            raise ValueError('this field is required')

class ForeignkeyRequiredWidget(widgets.ForeignKeyWidget):
    def clean(self, value, row=None, *args, **kwargs):
        if value:
            print(self.field, value)
            return self.get_queryset(value, row, *args, **kwargs).get(**{self.field: value})
        else:
            raise ValueError(self.field+ " required")

class PatientResource(resources.ModelResource):
    practitioner = fields.Field(column_name='practitioner', attribute='practitioner', widget=ForeignkeyRequiredWidget(Practitioner, 'id'),saves_null_values=False)
    first_Name = fields.Field(saves_null_values=False, column_name='first_Name', attribute='first_Name', widget=CharRequiredWidget())
    last_Name = fields.Field(saves_null_values=False, column_name='last_Name', attribute='last_Name', widget=CharRequiredWidget())
    gender = fields.Field(saves_null_values=False, column_name='gender', attribute='gender', widget=CharRequiredWidget())
    birthdate = fields.Field(saves_null_values=False, column_name='birthdate', attribute='birthdate', widget=CharRequiredWidget())
    family_number = fields.Field(saves_null_values=False, column_name='family_number', attribute='family_number', widget=CharRequiredWidget())

    class Meta:
        model = Patient
        fields = ('id', 'first_Name', 'last_Name', 'gender', 'birthdate', 'family_number')
        clean_model_instances = True

class PractitionerResource(resources.ModelResource):
    family_number = fields.Field(saves_null_values=False, column_name='family_number', attribute='family_number', widget=CharRequiredWidget())
    authorized_name = fields.Field(saves_null_values=False, column_name='authorized_name', attribute='authorized_name', widget=CharRequiredWidget())
    authorized_role = fields.Field(saves_null_values=False, column_name='authorized_role', attribute='authorized_role', widget=CharRequiredWidget())
    authorized_date = fields.Field(saves_null_values=False, column_name='authorized_date', attribute='authorized_date', widget=CharRequiredWidget())
    reporter_name = fields.Field(saves_null_values=False, column_name='reporter_name', attribute='reporter_name', widget=CharRequiredWidget())
    reporter_role = fields.Field(saves_null_values=False, column_name='reporter_role', attribute='reporter_role', widget=CharRequiredWidget())
    report_date = fields.Field(saves_null_values=False, column_name='report_date', attribute='report_date', widget=CharRequiredWidget())
    organization_name = fields.Field(column_name='organization_name', attribute='organization_name', widget=ForeignkeyRequiredWidget(Organization, 'id'),saves_null_values=False)

    class Meta:
        model = Practitioner
        fields = ('id', 'authorized_name' ,'authorized_role', 'authorized_date', 'reporter_name', 'reporter_role', 'report_date')
        clean_model_instances = True

class GenePanelResource(resources.ModelResource):
    # test_Name = fields.Field(saves_null_values=False, column_name='test_Name', attribute='test_Name', widget=CharRequiredWidget())
    gene_Name = fields.Field(saves_null_values=False, column_name='gene_Name', attribute='gene_Name', widget=CharRequiredWidget())
    zygosity = fields.Field(saves_null_values=False, column_name='zygosity', attribute='zygosity', widget=CharRequiredWidget())
    inheritance = fields.Field(saves_null_values=False, column_name='inheritance', attribute='inheritance', widget=CharRequiredWidget())
    hgvs_Description = fields.Field(saves_null_values=False, column_name='hgvs_Description', attribute='hgvs_Description', widget=CharRequiredWidget())
    classification = fields.Field(saves_null_values=False, column_name='classification', attribute='classification', widget=CharRequiredWidget())
    confirmed_Mutation = fields.Field(saves_null_values=False, column_name='confirmed_Mutation', attribute='confirmed_Mutation', widget=CharRequiredWidget())
    evidence_of_Classification = fields.Field(saves_null_values=False, column_name='evidence_of_Classification', attribute='evidence_of_Classification', widget=CharRequiredWidget())
    # gene_Description = fields.Field(saves_null_values=False, column_name='gene_Description', attribute='gene_Description', widget=CharRequiredWidget())
    # gene_References = fields.Field(saves_null_values=False, column_name='gene_References', attribute='gene_References', widget=CharRequiredWidget())
    comments = fields.Field(saves_null_values=False, column_name='comments', attribute='comments', widget=CharRequiredWidget())
    subject = fields.Field(column_name='first_Name', attribute='first_Name', widget=ForeignkeyRequiredWidget(Patient, 'id'),saves_null_values=False)
    specimen = fields.Field(column_name='specimen_type', attribute='specimen_type', widget=ForeignkeyRequiredWidget(Specimen, 'id'),saves_null_values=False)
    practitioner = fields.Field(column_name='authorized_name', attribute='authorized_name', widget=ForeignkeyRequiredWidget(Practitioner, 'id'),saves_null_values=False)

    class Meta:
        model = GenePanel
        fields = ('id', 'gene_Name', 'zygosity', 'inheritance', 'hgvs_Description', 'classification', 
        'confirmed_Mutation', 'evidence_of_Classification', 'comments')
        clean_model_instances = True

class SpecimenResource(resources.ModelResource):
    specimen_type = fields.Field(saves_null_values=False, column_name='specimen_type', attribute='specimen_type', widget=CharRequiredWidget())
    specimen_id = fields.Field(saves_null_values=False, column_name='specimen_id', attribute='specimen_id', widget=CharRequiredWidget())
    specimen_recieved_time = fields.Field(saves_null_values=False, column_name='specimen_recieved_time', attribute='specimen_recieved_time', widget=CharRequiredWidget())
    specimen_authorised_time = fields.Field(saves_null_values=False, column_name='specimen_authorised_time', attribute='specimen_authorised_time', widget=CharRequiredWidget())
    specimen_collection_time = fields.Field(saves_null_values=False, column_name='specimen_collection_time', attribute='specimen_collection_time', widget=CharRequiredWidget())
    patient = fields.Field(column_name='first_Name', attribute='first_Name', widget=ForeignkeyRequiredWidget(Patient, 'id'),saves_null_values=False)

    class Meta:
        model = Specimen
        fields = ('id','specimen_type','specimen_id','specimen_recieved_time', 'specimen_authorised_time' ,'specimen_collection_time')
        clean_model_instances = True

class OrganizationResource(resources.ModelResource):
    organization_Identifier = fields.Field(saves_null_values=False, column_name='organization_Identifier', attribute='organization_Identifier', widget=CharRequiredWidget())
    organization_Type = fields.Field(saves_null_values=False, column_name='organization_Type', attribute='organization_Type', widget=CharRequiredWidget())
    organization_Name = fields.Field(saves_null_values=False, column_name='organization_Name', attribute='organization_Name', widget=CharRequiredWidget())
    organization_Address_Line_1 = fields.Field(saves_null_values=False, column_name='organization_Address_Line_1', attribute='organization_Address_Line_1', widget=CharRequiredWidget())
    organization_address_line_2 = fields.Field(saves_null_values=False, column_name='organization_Address_Line_2', attribute='organization_Address_Line_2', widget=CharRequiredWidget())
    organization_City = fields.Field(saves_null_values=False, column_name='organization_City', attribute='organization_City', widget=CharRequiredWidget())
    organization_Post_Code = fields.Field(saves_null_values=False, column_name='organization_Post_Code', attribute='organization_Post_Code', widget=CharRequiredWidget())
    
    class Meta:
        model = Organization
        fields = ('id', 'organization_Identifier', 'organization_Type', 'organization_Name', 'organization_Address_Line_1', 'organization_Address_Line_2', 'organization_City', 'organization_Post_Code')
        clean_model_instances = True

class InterpretationResource(resources.ModelResource):
    report_Summary = fields.Field(saves_null_values=False, column_name='report_Summary', attribute='report_Summary', widget=CharRequiredWidget())
    subject = fields.Field(column_name='first_Name', attribute='first_Name', widget=ForeignkeyRequiredWidget(Patient, 'id'),saves_null_values=False)

    class Meta:
        model = OverallInterpretation
        fields = ('id', 'report_Summary')
        clean_model_instances = True

class RecommendationResource(resources.ModelResource):
    recommendation = fields.Field(saves_null_values=False, column_name='recommendation', attribute='recommendation', widget=CharRequiredWidget())
    subject = fields.Field(column_name='first_Name', attribute='first_Name', widget=ForeignkeyRequiredWidget(Patient, 'id'),saves_null_values=False)

    class Meta:
        model = Recommendation
        fields = ('id', 'recommendation')
        clean_model_instances = True

class ReferalReasonResource(resources.ModelResource):
    referal_Reason = fields.Field(saves_null_values=False, column_name='referal_Reason', attribute='referal_Reason', widget=CharRequiredWidget())
    subject = fields.Field(column_name='gene_Name', attribute='gene_Name', widget=ForeignkeyRequiredWidget(Patient, 'id'),saves_null_values=False)
    performer = fields.Field(column_name='authorized_name', attribute='authorized_name', widget=ForeignkeyRequiredWidget(Practitioner, 'id'),saves_null_values=False)

    class Meta:
        model = ReferalReason
        fields = ('id', 'referal_Reason')
        clean_model_instances = True

class TestMethodResource(resources.ModelResource):
    test_Method = fields.Field(saves_null_values=False, column_name='test_Method', attribute='test_Method', widget=CharRequiredWidget())
    gene_Information = fields.Field(saves_null_values=False, column_name='gene_Information', attribute='gene_Information', widget=CharRequiredWidget())
    gene_References = fields.Field(saves_null_values=False, column_name='gene_References', attribute='gene_References', widget=CharRequiredWidget())
    gene_Interpretation = fields.Field(column_name='report_Summary', attribute='report_Summary', widget=ForeignkeyRequiredWidget(OverallInterpretation, 'id'),saves_null_values=False)
    subject = fields.Field(column_name='first_Name', attribute='first_Name', widget=ForeignkeyRequiredWidget(Patient, 'id'),saves_null_values=False)

    class Meta:
        model = TestMethod
        fields = ('id', 'gene_Information', 'gene_References')
        clean_model_instances = True

class DiagnosticReportResource(resources.ModelResource):
    report_Title = fields.Field(saves_null_values=False, column_name='report_Title', attribute='report_Title', widget=CharRequiredWidget())
    conclusion = fields.Field(saves_null_values=False, column_name='conclusion', attribute='conclusion', widget=CharRequiredWidget())
    derived_From = fields.Field(column_name='gene_Name', attribute='gene_Name', widget=ForeignkeyRequiredWidget(GenePanel, 'id'),saves_null_values=False)
    performer = fields.Field(column_name='authorized_name', attribute='authorized_name', widget=ForeignkeyRequiredWidget(Practitioner, 'id'),saves_null_values=False)
    specimen = fields.Field(column_name='specimen_type', attribute='specimen_type', widget=ForeignkeyRequiredWidget(Specimen, 'id'),saves_null_values=False)

    class Meta:
        model = DiagnosticReport
        fields = ('id', 'report_Title', 'conclusion')
        clean_model_instances = True