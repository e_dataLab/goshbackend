# from django.shortcuts import render
from . models import Patient, Organization, Practitioner, Specimen, GenePanel, OverallInterpretation, DiagnosticReport, Recommendation, ReferalReason, TestMethod
from goshApp.serializer import PatientSerializer, PractitionerSerializer, OrganizationSerializer, SpecimenSerializer, GenePanelSerializer, OverallInterpretationSerializer, DiagnosticReportSerializer, TestMethodSerializer, ReferalReasonSerializer, RecommendationSerializer, FileSerializers
from rest_framework import generics, viewsets, status
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser, FormParser
from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework.response import Response
from django.http import HttpResponse
from . resources import PatientResource, PractitionerResource
from django.shortcuts import render

# from rest_framework.permissions import IsAuthenticated

# The search criteria in django suports these parameters
# '^' starts-with search
# '=' Exact matches
# '@' Full-test search. (currently supported in Django's Postgress backend)
# '$' Regex search

class PatientPagination(PageNumberPagination):
    page_size = 3

# Create your views here. these are also the endpoints
class PatientListView(generics.ListCreateAPIView):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    pagination_class = LimitOffsetPagination
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=first_Name', '=gender']

class PatientDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer   

class PractitionerListView(generics.ListCreateAPIView):
    queryset = Practitioner.objects.all()
    serializer_class = PractitionerSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=authorized_name', '^authorized_role','=authorized_role']

class PractitionerDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Practitioner.objects.all()
    serializer_class = PractitionerSerializer

class OrganizationListView(generics.ListCreateAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=organization_Name']

class OrganizationDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Organization.objects.all()
    serializer_class = OrganizationSerializer

class SpecimenListView(generics.ListCreateAPIView):
    queryset = Specimen.objects.all()
    serializer_class = SpecimenSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=specimen_id', '=specimen_type']

class SpecimenDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Specimen.objects.all()
    serializer_class = SpecimenSerializer

class GenePanelListView(generics.ListCreateAPIView):
    queryset = GenePanel.objects.all()
    serializer_class = GenePanelSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=gene_Name', '=zygosity', 'hgvs_description', '=classification']

class GenePanelDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = GenePanel.objects.all()
    serializer_class = GenePanelSerializer

class OverallInterpretationListView(generics.ListCreateAPIView):
    queryset = OverallInterpretation.objects.all()
    serializer_class = OverallInterpretationSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=report_Summary']

class OverallInterpretationDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = OverallInterpretation.objects.all()
    serializer_class = OverallInterpretationSerializer

class DiagnosticReportListView(generics.ListCreateAPIView):
    queryset = DiagnosticReport.objects.all()
    serializer_class = DiagnosticReportSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=conclusion']

class DiagnosticReportDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = DiagnosticReport.objects.all()
    serializer_class = DiagnosticReportSerializer

class RecommendationListView(generics.ListCreateAPIView):
    queryset = Recommendation.objects.all()
    serializer_class = RecommendationSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=recommendation']

class RecommendationDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Recommendation.objects.all()
    serializer_class = RecommendationSerializer

class ReferalReasonListView(generics.ListCreateAPIView):
    queryset = ReferalReason.objects.all()
    serializer_class = ReferalReasonSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=referal_Reason']

class ReferalReasonDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = ReferalReason.objects.all()
    serializer_class = ReferalReasonSerializer

class TestMethodListView(generics.ListCreateAPIView):
    queryset = TestMethod.objects.all()
    serializer_class = TestMethodSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['=id', '=test_Method']

class TestMethodDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = TestMethod.objects.all()
    serializer_class = TestMethodSerializer

class FileUploadView(APIView):
    permission_class = []
    parser_class = (FileUploadParser, )

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializers(data=request.data)

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(file_serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
