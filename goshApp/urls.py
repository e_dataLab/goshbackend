from django.urls import path
from goshApp import views
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from goshApp import views as FileViews

urlpatterns = [
    path('Patient/', views.PatientListView.as_view()),
    path('Patient/<int:pk>', views.PatientDetailView.as_view()),

    path('Practitioner/', views.PractitionerListView.as_view()),
    path('Practitioner/<int:pk>', views.PractitionerDetailView.as_view()),

    path('Organization/', views.OrganizationListView.as_view()),
    path('Organization/<int:pk>', views.OrganizationDetailView.as_view()),

    path('Specimen/', views.SpecimenListView.as_view()),
    path('Specimen/<int:pk>', views.SpecimenDetailView.as_view()),

    path('Genepanel/', views.GenePanelListView.as_view()),
    path('Genepanel/<int:pk>', views.GenePanelDetailView.as_view()),

    path('DiagnosticReport/', views.DiagnosticReportListView.as_view()),
    path('DiagnosticReport/<int:pk>', views.DiagnosticReportDetailView.as_view()),

    path('Interpretation/', views.OverallInterpretationListView.as_view()),
    path('Interpretation/<int:pk>', views.OverallInterpretationDetailView.as_view()),

    path('Recommendation/', views.RecommendationListView.as_view()),
    path('Recommendation/<int:pk>', views.RecommendationDetailView.as_view()),

    path('ReferalReason/', views.ReferalReasonListView.as_view()),
    path('ReferalReason/<int:pk>', views.ReferalReasonDetailView.as_view()),

    path('TestMethod/', views.TestMethodListView.as_view()),
    path('TestMethod/<int:pk>', views.TestMethodDetailView.as_view()),

    path('uploads', FileViews.FileUploadView.as_view()),

    path('api-token-auth/', obtain_auth_token, name="api-token-auth")
]
