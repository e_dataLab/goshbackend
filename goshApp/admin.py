from django.contrib import admin
from . models import Practitioner, Patient, GenePanel, Specimen, DiagnosticReport, Organization, Recommendation, TestMethod, OverallInterpretation, ReferalReason
from import_export.admin import ImportExportModelAdmin
from . resources import PatientResource, PractitionerResource, GenePanelResource, TestMethodResource, SpecimenResource, OrganizationResource, ReferalReasonResource, RecommendationResource, InterpretationResource, DiagnosticReportResource

# Register your models here.
# @admin.register(Patient)
class PatientAdmin(ImportExportModelAdmin):
    resource_class = PatientResource
    list_display = ('id', 'first_Name', 'last_Name', 'gender', 'birthdate', 'family_number')

admin.site.register(Patient, PatientAdmin)

class PractitionerAdmin(ImportExportModelAdmin):
    resource_class = PractitionerResource
    list_display = ('id', 'authorized_name','authorized_role', 'authorized_date', 'reporter_name', 'reporter_role', 'report_date')

admin.site.register(Practitioner, PractitionerAdmin)

class GenePanelAdmin(ImportExportModelAdmin):
    resource_class = GenePanelResource
    list_display = ('id', 'gene_Name', 'zygosity', 'inheritance', 'hgvs_Description', 'classification', 
        'confirmed_Mutation', 'evidence_of_Classification', 'comments')
admin.site.register(GenePanel, GenePanelAdmin)

class SpecimenAdmin(ImportExportModelAdmin):
    resource_class = SpecimenResource
    list_display = ('id','specimen_type','specimen_id','specimen_recieved_time','specimen_authorised_time','specimen_collection_time')
admin.site.register(Specimen, SpecimenAdmin)

class ReferalReasonAdmin(ImportExportModelAdmin):
    resource_class = ReferalReasonResource
    list_display = ('id', 'referal_Reason')
admin.site.register(ReferalReason, ReferalReasonAdmin)

class RecommendationAdmin(ImportExportModelAdmin):
    resource_class = RecommendationResource
    list_display = ('id', 'recommendation')
admin.site.register(Recommendation, RecommendationAdmin)

class TestMethodAdmin(ImportExportModelAdmin):
    resource_class = TestMethodResource
    list_display = ('id', 'gene_Information', 'gene_References')
admin.site.register(TestMethod, TestMethodAdmin)

class InterpretationAdmin(ImportExportModelAdmin):
    resource_class = InterpretationResource
    list_display = ('id', 'report_Summary')
admin.site.register(OverallInterpretation, InterpretationAdmin)

class DiagnosticReportAdmin(ImportExportModelAdmin):
    resource_class = DiagnosticReportResource
    list_display = ('id', 'report_Title', 'conclusion')
admin.site.register(DiagnosticReport, DiagnosticReportAdmin)

class OrganizationAdmin(ImportExportModelAdmin):
    resource_class = OrganizationResource
    list_display = ('id', 'organization_Identifier', 'organization_Type', 'organization_Name', 'organization_Address_Line_1', 'organization_Address_Line_2', 'organization_City', 'organization_Post_Code')
admin.site.register(Organization, OrganizationAdmin)