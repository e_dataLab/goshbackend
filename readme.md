How to run the code

System requirements.

    Ensure you have python 3.10 installed in your PC / Laptop.

    Download and install postgresql on your PC / Laptop.

        log into an interactive postgres session

        create database for your project

            postgres=# CREATE DATABASE myproject;

        Next, create a database user for our project. Make sure to select a secure password:

            postgres=# CREATE USER myprojectuser WITH PASSWORD 'password';
        
        Afterwards, we’ll modify a few of the connection parameters for the user we just created. This will speed up database operations so that the correct values do not have to be queried and set each time a connection is established.
        We are setting the default encoding to UTF-8, which Django expects. We are also setting the default transaction isolation scheme to “read committed”, which blocks reads from uncommitted transactions. Lastly, we are setting the timezone. By default, our Django projects will be set to use UTC. These are all recommendations from the Django project itself:
            
            postgres=# ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
            postgres=# ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
            postgres=# ALTER ROLE myprojectuser SET timezone TO 'UTC';
        
        Now, we can give our new user access to administer our new database:

            postgres=# GRANT ALL PRIVILEGES ON DATABASE myproject TO myprojectuser;

        When you are finished, exit out of the PostgreSQL prompt by typing:

            postgres=# \q

In the settings.py file, you can modify the database details with your postgresql configurations by going to DATABASES and change NAME, USER and PASSWORD

However to view your database and data, you can:

    Download and install pgAdmin

Once the installation above is running, you can clone the application from GitLab using these steps:

First, perform a git clone from the repo

Modify the file path of line 18 of requiremnts.txt to your file path 

Run pip install requirements.txt, 

if all the modules are downloaded and installed properly, you can then run these in your prefered code editor.

python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py runserver

Go to your browser and use http://localhost:8000/dre-api/Patient/ to access your patient demographics 
    Other api's can be accessed by going to the api's as follows
            dre-api/ Patient/id
            dre-api/ Practitioner/
            dre-api/ Practitioner/id
            dre-api/ Organization/
            dre-api/ Organization/id
            dre-api/ Specimen/
            dre-api/ Specimen/id
            dre-api/ Genepanel/
            dre-api/ Genepanel/id
            dre-api/ DiagnosticReport/
            dre-api/ DiagnosticReport/id
            dre-api/ Interpretation/
            dre-api/ Interpretation/id
            dre-api/ Recommendation/
            dre-api/ Recommendation/id
            dre-api/ ReferalReason/
            dre-api/ ReferalReason/id
            dre-api/ TestMethod/
            dre-api/ TestMethod/id

Note, its preferrable to work on a virtual environment.

To run the Hapi FHIR server, use the following steps.

Download and install docker on your machine.

save the docker-compose.yml file in your prefered location on a virtual environment and ensure postgresql is installed on your Laptop / PC.

Run 

docker-compose up to start the API

and 

docker-compose down to shutdown the server.

To access your api, go to your prefered browser or use Postman to run

http://localhost:8090/fhir/metadata for CapabilityStatement



#Connect React or Angula framework

To connect your React or Angula framework to the django application, use http://localhost:4200 